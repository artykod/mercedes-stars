﻿using UnityEngine;
using System.Collections;

/// <summary>
/// компонент реализовывает рендеринг пунктирной линии независимо от ее поворота в 3д-пространстве
/// эдакая билбордная линия, рисуется в одной плоскости, потому оптимально в плане draw calls
/// </summary>
[
	RequireComponent(
		typeof(MeshRenderer), 
		typeof(MeshFilter)
	)
]
public class LineRenderer : MonoBehaviour {
	private MeshFilter meshFilter = null;
	private Mesh mesh = null;

	private Vector3[] vertices = null;
	private Vector2[] uvs = null;
	private Vector3[] points = null;
	private float width = 1f;
	private float zPosition = 0f;

	private void Awake() {
		meshFilter = GetComponent<MeshFilter>();
	}

	public void Build(Vector3[] points, float width, float zPosition) {
		this.points = points.Clone() as Vector3[];
		this.width = width;
		this.zPosition = zPosition;

		vertices = new Vector3[(points.Length - 1) * 4];
		uvs = new Vector2[vertices.Length];
		int[] indices = new int[(points.Length - 1) * 6];

		for (int i = 0; i < points.Length - 1; i++) {
			int index = i * 6;
			int indexVertices = i * 4;
			indices[index + 0] = indexVertices + 0;
			indices[index + 1] = indexVertices + 1;
			indices[index + 2] = indexVertices + 2;
			indices[index + 3] = indexVertices + 2;
			indices[index + 4] = indexVertices + 3;
			indices[index + 5] = indexVertices + 0;
		}

		mesh = new Mesh();
		mesh.vertices = vertices;
		mesh.triangles = indices;
		mesh.uv = uvs;
		mesh.RecalculateBounds();

		meshFilter.sharedMesh = mesh;
	}

	public void SetPosition(int index, Vector3 position) {
		points[index] = position;
	}

	public void Refresh(Quaternion rotation, Vector3 scale) {
		for (int i = 0; i < points.Length - 1; i++) {
			Vector3 a = points[i];
			Vector3 b = points[i + 1];

			a.x *= scale.x;
			a.y *= scale.y;
			a = rotation * a;

			b.x *= scale.x;
			b.y *= scale.y;
			b = rotation * b;

			a.z = b.z = zPosition;

			Vector3 n = b - a;
			float length = n.magnitude;

			n /= length;
			float tmp = n.y;
			n.y = n.x;
			n.x = -tmp;
			n *= width * 0.5f;

			int index = i * 4;

			vertices[index + 0] = a - n;
			vertices[index + 1] = b - n;
			vertices[index + 2] = b + n;
			vertices[index + 3] = a + n;

			float u = (int)(length / width);

			uvs[index + 0] = Vector2.zero;
			uvs[index + 1] = new Vector2(u, 0f);
			uvs[index + 2] = new Vector2(u, 1f);
			uvs[index + 3] = Vector2.up;

			mesh.vertices = vertices;
			mesh.uv = uvs;
		}
	}
}
