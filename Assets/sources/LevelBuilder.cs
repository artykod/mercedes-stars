﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelBuilder : MonoBehaviour {

	private class LineInfo {
		public LineRenderer line = null;
	}

	private class StarInfo {
		public GameObject star = null;
		public Vector3 position = Vector3.zero;
	}

	[SerializeField]
	private LineRenderer linePrefab = null;
	[SerializeField]
	private GameObject starPrefab = null;
	[SerializeField]
	private Material carMaterial = null;
	[SerializeField]
	private Material backgroundMaterial = null;
	[SerializeField]
	private Material glowMaterial = null;
	[SerializeField]
	private Material starMaterial = null;
	[SerializeField]
	private Material startScreenMaterial = null;
	[SerializeField]
	private Texture2D[] backgrounds = null;
	[SerializeField]
	private Texture2D[] cars = null;

	private Vector3 screenSize;
	private Levels levels = null;

	private int[] backgroundsIndices = new int[] {
		1, // level 1
		0, // level 2
		1, // level 3
		0, // level 4
		1, // level 5
	};
	private string[] carTitles = new string[] {
		"Mercedes-Benz A-Класс",
		"Mercedes-Benz CLA-Класс",
		"Mercedes-Benz GLA-Класс",
		"Mercedes-Benz C-Класс",
		"Mercedes-Benz S-Класс Coupe",
	};
	private Color[] carTitleColors = new Color[] {
		Color.white,
		Color.black,
		Color.white,
		Color.black,
		Color.black,
	};

	private List<LineInfo> lines = new List<LineInfo>(100);
	private List<StarInfo> stars = new List<StarInfo>(100);

	private static int starsCounter = 0;

	public string CurrentCarTitle { get; private set; }
	public Color CurrentCarTitleColor { get; private set; }

	public int LevelsCount { get { return levels != null ? levels.GetLevelsCount() : 0; } }

	public Material CarMaterial { get { return carMaterial; } }
	public Material BackgroundMaterial { get { return backgroundMaterial; } }
	public Material LinesMaterial { get { return glowMaterial; } }
	public Material StarsMaterial { get { return starMaterial; } }
	public Material StartScreenMaterial { get { return startScreenMaterial; } }


	public void LoadLevel(int index) {
		int levelIndex = index;
		foreach (var i in levels.GetLevel(levelIndex)) {
			BuildPolyLineFromPoints(i);
		}
		carMaterial.mainTexture = cars[levelIndex];
		backgroundMaterial.mainTexture = backgrounds[backgroundsIndices[levelIndex]];
		CurrentCarTitle = carTitles[levelIndex];
		CurrentCarTitleColor = carTitleColors[levelIndex];
	}

	public void TransformObjectPositions(Transform tr) {
		Quaternion rotation = tr.rotation;
		Vector3 scale = tr.localScale;

		foreach (var l in lines) {
			l.line.Refresh(rotation, scale);
		}

		foreach (var s in stars) {
			Vector3 p = s.position;
			p.x *= scale.x;
			p.y *= scale.y;
			p = rotation * p;
			p.z = -1.5f;
			s.star.transform.position = p;
		}
	}

	private void Awake() {
		screenSize = Camera.main.ViewportToWorldPoint(Vector3.one);

		float scale = screenSize.y / 1080f * 2f;
		transform.localScale = new Vector3(scale, scale, 1f);

		levels = new Levels();
	}

	private void BuildPolyLineFromPoints(Vector3[] points) {
		LineRenderer line = Instantiate(linePrefab);
		line.Build(points, 0.08f, -1f);

		LineInfo lineInfo = new LineInfo();
		lineInfo.line = line;

		for (int i = 0; i < points.Length; i++) {
			Vector3 point = TransformPoint(points[i]);

			line.SetPosition(i, point);

			if ((starsCounter % 10) == 0) {
				GameObject star = Instantiate(starPrefab);
				star.transform.SetParent(transform, false);
				star.transform.localPosition = point;
				star.transform.SetParent(null, true);
				star.transform.localScale = Vector3.one;

				StarInfo starInfo = new StarInfo();
				starInfo.star = star;
				starInfo.position = point;

				stars.Add(starInfo);
			}
			starsCounter++;
		}

		lines.Add(lineInfo);
	}

	private Vector3 TransformPoint(Vector3 p) {
		p.y = -p.y;
		return p;
	}
}
